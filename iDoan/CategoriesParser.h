//
//  XMLParser.h
//  XMLParserTutorial
//
//  Created by Kent Franks on 5/6/11.
//  Copyright 2011 TheAppCodeBlog. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryGame.h"


@interface CategoriesParser : NSObject <NSXMLParserDelegate>
{
	NSMutableArray	*categories;
	NSXMLParser		*parser;
}

@property (readonly, retain) NSMutableArray	*categories;

-(id) loadXMLByURL:(NSString *)urlString;
-(id) loadXMLLocal:(NSString *)fileName;

@end
