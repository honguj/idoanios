//
//  Popup.h
//  TouchHandling
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"
#import "cocos2d-ui.h"
#import "MainMenuScene.h"

@interface DualModePopup : CCSprite

- (void) loadInScene:(MainMenuScene *)p;

@end
