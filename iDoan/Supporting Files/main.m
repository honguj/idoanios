//
//  main.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright Tic Toc Prod 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
        return retVal;
    }
}
