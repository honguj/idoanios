//
//  XMLParser.m
//  XMLParserTutorial
//
//  Created by Kent Franks on 5/6/11.
//  Copyright 2011 TheAppCodeBlog. All rights reserved.
//

#import "CategoriesParser.h"
#import "CategoryParser.h"

@implementation CategoriesParser
@synthesize categories;

-(id) loadXMLByURL:(NSString *)urlString
{
	categories			= [[NSMutableArray alloc] init];
	NSURL *url		= [NSURL URLWithString:urlString];
	NSData	*data   = [[NSData alloc] initWithContentsOfURL:url];
	parser			= [[NSXMLParser alloc] initWithData:data];
	parser.delegate = self;
	[parser parse];
	return self;
}

-(id) loadXMLLocal:(NSString *)fileName
{
    categories			= [[NSMutableArray alloc] init];

    // Read data from XML
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"xml"];
    
    // Create NSData instance from xml in filePath
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];

	parser			= [[NSXMLParser alloc] initWithData:xmlData];
	parser.delegate = self;
	[parser parse];
	return self;
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if ([elementname isEqualToString:@"category"])
	{
        CategoryGame *cat = [[CategoryGame alloc] initWithName:[attributeDict objectForKey:@"name"] hash:[attributeDict objectForKey:@"id"]];
        cat.instruction = [attributeDict objectForKey:@"instruction"];
        cat.description = [attributeDict objectForKey:@"mode"];
        [categories addObject:cat];
	}
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{

}

- (void) dealloc
{
//	[parser release];
//	[super dealloc];
}

@end
