//
//  Popup.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "DualModePopup.h"
#import "CategoriesScene.h"
#import "GameManager.h"
#import "GameSupport.h"
#import "ConstantManager.h"
#import "Match.h"

@interface DualModePopup(){
    NSMutableArray* teamOptions;
    NSMutableArray* roundOptions;
    MainMenuScene *parent;
    int numberOfTeams;
    int numberOfRounds;
    CCColor* selectedColor;
    CCColor* unselectedColor;
}

@end


@implementation DualModePopup

- (void) loadInScene:(MainMenuScene *)p;{
    // Init class variables
    parent = p;
    selectedColor = [CCColor colorWithRed:0.5 green:0.5 blue:0.5];
    unselectedColor = [CCColor colorWithRed:1.0 green:1.0 blue:1.0];
    
    self.name = @"dualPopup";
    teamOptions = [[NSMutableArray alloc] init];
    roundOptions= [[NSMutableArray alloc] init];
    numberOfTeams = [ConstantManager TEAMS_MIN];
    numberOfRounds = [ConstantManager ROUNDS_MIN];
    
    // Title label
    CCLabelTTF *title= [CCLabelTTF labelWithString:@"THI ĐẤU" fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager POPUP_TITLE_TEXT_SIZE]];
    title.color = [CCColor colorWithRed:1.0f green:0.0f blue:0.0f];
    title.positionType = CCPositionTypeNormalized;
    title.position = ccp(0.5f, 0.9f);
    [self addChild:title z:0 name:@"title"];
    
    // Create buttons
    CCButton* closeButton = [CCButton buttonWithTitle:@"" spriteFrame:[ConstantManager POPUP_CLOSE_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager POPUP_CLOSE_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    closeButton.positionType = CCPositionTypeNormalized;
    closeButton.position = ccp(0.98f, 0.98f);
    closeButton.block = ^(id sender)
    {
        // close button selected
        [GameSupport playSound:BUTTON_CLICK];
        [parent removeChild:self];
        parent.userInteractionEnabled = NO;
    };
    [self addChild:closeButton z:1 name:@"close"];
    
    CCButton* playButton = [CCButton buttonWithTitle:@"Bắt đầu" spriteFrame:[ConstantManager POPUP_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager POPUP_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    playButton.label.fontName = [ConstantManager MAIN_FONT_NAME];
    playButton.label.fontSize = [ConstantManager MENU_BUTTON_FONT_SIZE];
    playButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    playButton.positionType = CCPositionTypeNormalized;
    playButton.position = ccp(0.5f, 0.15f);
    playButton.block = ^(id sender)
    {
        // play button selected
        [GameSupport playSound:BUTTON_CLICK];
        [parent removeChild:self];
        
        // setup match with selected options
        [[GameManager sharedManager] setNumberOfRounds:numberOfRounds];
        [[GameManager sharedManager] setNumberOfTeams:numberOfTeams];
        
        CCTransition* t = [CCTransition transitionFadeWithDuration:0.1f];
        [[CCDirector sharedDirector] replaceScene:[CategoriesScene scene]
                                   withTransition:t];
    };
    [self addChild:playButton z:1 name:@"play"];
    
    // Load team options
    [self loadTeamOption];
    
    // Load round options
    [self loadRoundOption];
}

- (void) loadRoundOption{
    
    // title
    CCLabelTTF *teamTitle= [CCLabelTTF labelWithString:@"Số vòng:" fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager POPUP_DUAL_TEXT_SIZE]];
    teamTitle.color = [GameSupport colorFromHexString:[ConstantManager CAT_BUTTON_COLOR_CODE]];
    teamTitle.positionType = CCPositionTypeNormalized;
    teamTitle.position = ccp(0.28f, 0.5f);
    [self addChild:teamTitle z:1 name:@"roundTitle"];
    
    // Options
    float positionX = 0.3f;
    float positionY = 0.35f;
    for (int i = [ConstantManager ROUNDS_MIN]; i <= [ConstantManager ROUNDS_MAX]; i+=2){
        // Empty box
        CCSprite *option = [self blankSpriteWithSize:CGSizeMake([ConstantManager POPUP_OPTION_BOX_SIZE], [ConstantManager POPUP_OPTION_BOX_SIZE]) name:[NSString stringWithFormat:@"%d", i]];
        if ( i == [ConstantManager ROUNDS_MIN]) // Select minRounds by default
            option.color = selectedColor;
        else
            option.color = unselectedColor;
        option.positionType = CCPositionTypeNormalized;
        option.position = ccp(positionX, positionY);
        [self addChild:option z:1 name:option.name];
        
        // Label
        CCLabelTTF *label = [CCLabelTTF labelWithString: [NSString stringWithFormat:@"%d", i] fontName:@"Arial Rounded MT Bold" fontSize:[ConstantManager POPUP_TEXT_SIZE]];
        label.color = [CCColor colorWithRed:0.0f green:0.0f blue:0.0f];
        label.positionType = CCPositionTypeNormalized;
        label.position = ccp(positionX, positionY);
        [self addChild:label z:2];
        
        // Add to list
        [roundOptions addObject:option];
        
        positionX += 0.2f;
    }

}

- (void) loadTeamOption{
    // title
    CCLabelTTF *teamTitle= [CCLabelTTF labelWithString:@"Số đội chơi:" fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager POPUP_DUAL_TEXT_SIZE]];
    teamTitle.color = [GameSupport colorFromHexString:[ConstantManager CAT_BUTTON_COLOR_CODE]];
    teamTitle.positionType = CCPositionTypeNormalized;
    teamTitle.position = ccp(0.3f, 0.8f);
    [self addChild:teamTitle z:1 name:@"teamTitle"];
    
    // Options
    float positionX = 0.3f;
    float positionY = 0.65;
    for (int i = [ConstantManager TEAMS_MIN]; i <= [ConstantManager TEAMS_MAX]; i++){
        // Empty box
        CCSprite *option = [self blankSpriteWithSize:CGSizeMake([ConstantManager POPUP_OPTION_BOX_SIZE], [ConstantManager POPUP_OPTION_BOX_SIZE]) name:[NSString stringWithFormat:@"%d", i]];
        if ( i == [ConstantManager TEAMS_MIN]) // Select minTeams by default
            option.color = selectedColor;
        else
            option.color = unselectedColor;
        //[CCColor colorWithRed:1.0 green:1.0 blue:1.0];
        option.positionType = CCPositionTypeNormalized;
        option.position = ccp(positionX, positionY);
        [self addChild:option z:1 name:option.name];
        
        // Label
        CCLabelTTF *label = [CCLabelTTF labelWithString: [NSString stringWithFormat:@"%d", i] fontName:@"Arial Rounded MT Bold" fontSize:[ConstantManager POPUP_TEXT_SIZE]];
        label.color = [CCColor colorWithRed:0.0f green:0.0f blue:0.0f];
        label.positionType = CCPositionTypeNormalized;
        label.position = ccp(positionX, positionY);
        [self addChild:label z:2];
        
        // Add to list
        [teamOptions addObject:option];
        
        positionX += 0.2f;
    }
    
}

- (CCSprite*)blankSpriteWithSize:(CGSize)size name:(NSString *)name
{
    CCSprite *sprite = [CCSprite node];
    GLubyte *buffer = malloc(sizeof(GLubyte)*4);
    for (int i=0;i<4;i++) {buffer[i]=255;}
    CCTexture *tex = [[CCTexture alloc] initWithData:buffer pixelFormat:CCTexturePixelFormat_RGB5A1 pixelsWide:1 pixelsHigh:1 contentSizeInPixels:size contentScale:1.0f];
    [sprite setTexture:tex];
    [sprite setTextureRect:CGRectMake(0, 0, size.width, size.height)];
    sprite.name = name;
    free(buffer);
    return sprite;
}

-(void) updateColor:(CCSprite*) selectedBox in:(NSMutableArray*)collection{
    for (CCSprite* s in collection){
        if (s == selectedBox)
            s.color = selectedColor;
        else
            s.color = unselectedColor;
    }
}

- (void)onEnter {
    self.userInteractionEnabled = TRUE;
}

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint location = [touch locationInView: [touch view]];
    location = [[CCDirector sharedDirector] convertToGL: location];
    CGPoint convertedNodeSpacePoint = [self convertToNodeSpace:location];

    for (CCSprite* s in teamOptions){
        if (CGRectContainsPoint(s.boundingBox, convertedNodeSpacePoint)){
            [self updateColor:s in:teamOptions];
            numberOfTeams = [s.name integerValue];
            break;
        }
    }
    
    for (CCSprite* s in roundOptions){
        if (CGRectContainsPoint(s.boundingBox, convertedNodeSpacePoint)){
            [self updateColor:s in:roundOptions];
            numberOfRounds = [s.name integerValue];
            break;
        }
    }
}

- (void)touchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{

}

@end
