//
//  LoadingPopup.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-23.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface LoadingPopup : CCSprite

- (void) loadInScene:(CCScene *)p;
- (void) closePopup;

@end
