//
//  Result.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Result : NSObject

@property (nonatomic, retain) NSString *guessingWord;
@property (nonatomic) bool isCorrect;

- (id) initWithWord:(NSString *)word correct:(bool)c;

@end
