//
//  IntroScene.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright Tic Toc Prod 2014. All rights reserved.
//
// -----------------------------------------------------------------------

// Import the interfaces
#import "IntroScene.h"
#import "HelloWorldScene.h"
#import "MainMenuScene.h"
#include "CCActionInterval.h"
#import "GameManager.h"
#import "LoadingPopup.h"

// -----------------------------------------------------------------------
#pragma mark - IntroScene
// -----------------------------------------------------------------------

@implementation IntroScene

// -----------------------------------------------------------------------
#pragma mark - Create & Destroy
// -----------------------------------------------------------------------

+ (IntroScene *)scene
{
	return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Create a colored background (Dark Grey)
    CCNodeColor *background = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:1.0f]];
    [self addChild:background];
    
    // Logo background
    CCSprite *logoBackground = [CCSprite spriteWithImageNamed:@"splash.png"];
    logoBackground.position  = ccp(self.contentSize.width/2,self.contentSize.height/2);
    [self addChild:logoBackground];

    CCActionDelay *delayAction = [CCActionDelay actionWithDuration:2.0];
    CCTransition* t = [CCTransition transitionFadeWithDuration:0.5f];
    t.outgoingSceneAnimated = YES;
    t.incomingSceneAnimated = YES;
    CCActionCallBlock *nextSceneAction = [CCActionCallBlock actionWithBlock:^{
        [[CCDirector sharedDirector] replaceScene:[MainMenuScene scene]
                                   withTransition:t];
    }];
    
    CCActionCallBlock *loadingCatAction = [CCActionCallBlock actionWithBlock:^{
        // Load all categories
        [[GameManager sharedManager] loadAllCategories];
    }];
    
    CCActionCallBlock *loadingSceneAction = [CCActionCallBlock actionWithBlock:^{
        // Loading popup
        LoadingPopup* popup = [LoadingPopup spriteWithImageNamed:@"CategoryBackground.png"];
        [self addChild:popup z:2];
        popup.positionType = CCPositionTypeNormalized;
        popup.position = ccp(0.5f, 0.5f);
//        popup.color = [CCColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f];
        [popup loadInScene:self];
    }];
    
    id sequence = [CCActionSequence actions:delayAction, loadingSceneAction, delayAction, loadingCatAction, nextSceneAction, nil];
    [self runAction:sequence];

    
    // done
	return self;
}

// -----------------------------------------------------------------------
@end
