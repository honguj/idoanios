//
//  GameScene.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"
#import "cocos2d-ui.h"
#include <CoreMotion/CoreMotion.h>
#import <CoreFoundation/CoreFoundation.h>

@interface GameScene : CCScene{
    CMMotionManager *motionManager;
    float rollAngle;
    CCLabelTTF *yawLabel; // Test - TO REMOVE
}

@property (nonatomic, retain) CMMotionManager *motionManager;

+ (GameScene *)scene;
- (id)init;
@end
