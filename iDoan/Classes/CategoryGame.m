//
//  Category.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CategoryGame.h"

@implementation CategoryGame

@synthesize name;
@synthesize hash;
@synthesize instruction;
@synthesize description;
@synthesize words;

static NSString* NAME = @"CatName";
static NSString* HASH = @"CatHash";
static NSString* INSTRUCTION = @"CatIns";
static NSString* DESCRIPTION = @"CatDesc";

-(id) initWithName:(NSString*)n hash:(NSString*)h{
    self = [super init];
    if (!self) return(nil);
    
    self.name = n;
    self.hash = h;
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:name   forKey:NAME];
    [encoder encodeObject:hash forKey:HASH];
    [encoder encodeObject:instruction forKey:INSTRUCTION];
    [encoder encodeObject:description forKey:DESCRIPTION];
    [encoder encodeObject:words forKey:@"test"];
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [self init];
    self.name = [coder decodeObjectForKey:NAME];
    self.hash = [coder decodeObjectForKey:HASH];
    self.instruction = [coder decodeObjectForKey:INSTRUCTION];
    self.description = [coder decodeObjectForKey:DESCRIPTION];
    self.words = nil;
    
    return self;
}

@end
