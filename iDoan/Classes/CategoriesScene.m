//
//  CategoriesScene.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CategoriesScene.h"
#import "MainMenuScene.h"
#import "GameScene.h"
#import "InstructionPopup.h"
#import "GameManager.h"
#import "ConstantManager.h"
#import "GameSupport.h"
#import "Match.h"
#import "CategoryGame.h"

@interface CategoriesScene(){
    NSArray *categories;
}

@end

@implementation CategoriesScene

static int buttonHeight = 45;
static int buttonWidth = 120;

+ (CategoriesScene *)scene
{
	return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Create a colored background
    CCSprite *background = [CCSprite spriteWithImageNamed:@"CategoryBackground.png"];
    background.position  = ccp(self.contentSize.width/2,self.contentSize.height/2);
    [self addChild:background];

    
    // Init data
    categories = [[GameManager sharedManager] categories];
    
    [self loadCategoryGrid];
    
    [self loadHUD];
    
    // Popup ads
    [GameSupport popupAdsInScene:self within:1.0f and:2.5f];
    
    // Google analytics
    [GameSupport trackSceneWithName:@"CategoriesScene"];
    
    return self;
}

- (void) loadHUD{
    // Create a back button
    CCButton *backButton = [CCButton buttonWithTitle:@"Thoát" spriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    backButton.label.fontName = [ConstantManager HEADER_BUTTON_FONT_NAME];
    backButton.label.fontSize = [ConstantManager HEADER_BUTTON_FONT_SIZE];
    backButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    backButton.positionType = CCPositionTypeNormalized;
    backButton.color = [CCColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    backButton.position = ccp([ConstantManager BACK_BUTTON_X], [ConstantManager BACK_BUTTON_Y]); // Top Left of screen
    [backButton setTarget:self selector:@selector(onBackClicked:)];
    [self addChild:backButton];
    
    // Header Label
    CCLabelTTF* header = [CCLabelTTF labelWithString:@"Chủ đề" fontName:[ConstantManager MAIN_FONT_NAME] fontSize:40* [ConstantManager FONT_SIZE_SCALE_RATIO]];
    header.color = [GameSupport colorFromHexString:@"#A71E22"];
    header.positionType = CCPositionTypeNormalized;
    header.position = ccp(0.5, [ConstantManager BACK_BUTTON_Y]);
    [self addChild:header];
}

- (void) loadCategoryGrid{
    float numberRows = ceil(categories.count*1.0/[ConstantManager COLUMNS_PER_SCREEN]);
    float spacingX = (([CCDirector sharedDirector].viewSize.width/[ConstantManager COLUMNS_PER_SCREEN]) - buttonWidth)/2;
    float spacingY = ((([CCDirector sharedDirector].viewSize.height - [ConstantManager CATEGORY_HUD_HEIGHT])/ [ConstantManager ROWS_PER_SCREEN])- buttonHeight)/2;
    
    CCNode *cats = [CCNode node];
    float catsHeight = ceil(numberRows / [ConstantManager ROWS_PER_SCREEN]) * [CCDirector sharedDirector].viewSize.height;
    cats.contentSize = CGSizeMake([CCDirector sharedDirector].viewSize.width, catsHeight);
    
    CCSpriteFrame *imageOff = [CCSpriteFrame frameWithImageNamed:@"CategoryButtonNormal.png"];
    CCSpriteFrame *imageOn = [CCSpriteFrame frameWithImageNamed:@"CategoryButtonClicked.png"];

    int posX = spacingX + buttonWidth/2;
    int posY = cats.contentSize.height - spacingX - buttonHeight/2;

    int index = 0;
    for(int row=0; row < numberRows; row++){
        if (row % [ConstantManager ROWS_PER_SCREEN] == 0)
            posY -= [ConstantManager CATEGORY_HUD_HEIGHT];
        for (int col = 0; col < [ConstantManager COLUMNS_PER_SCREEN]; col++){
            index = row * [ConstantManager COLUMNS_PER_SCREEN] + col;
            if (index >= categories.count)
                break;
            CategoryGame* cat = categories[index];
            
            // Category button
            CCButton* button = [CCButton buttonWithTitle:cat.name spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
            button.label.fontName = [ConstantManager MENU_BUTTON_FONT_NAME];
            button.label.fontSize = [ConstantManager MENU_BUTTON_FONT_SIZE];
            button.label.fontColor = [GameSupport colorFromHexString:[ConstantManager CAT_BUTTON_COLOR_CODE]];
            button.position = ccp(posX, posY);
            button.contentSize = CGSizeMake(buttonWidth, buttonHeight);
            button.block = ^(id sender)
            {
                // Send data to google analytic
                [GameSupport trackEventWithCategory:@"CategoriesScene" action:@"CategorySelected" label:cat.name];
                
                [GameSupport playSound:BUTTON_CLICK];
                // set selectec cat
                Match* m = [[GameManager sharedManager] match];
                m.category = cat;
                
                // Load words list
                [[GameManager sharedManager] loadCategoryWords:cat.hash];
                
                // instruction popup
                InstructionPopup* popup = [InstructionPopup spriteWithImageNamed:@"PopupBackground.png"];
                [self addChild:popup z:2];
                popup.positionType = CCPositionTypeNormalized;
                popup.position = ccp(0.5f, 0.55f);
                [popup loadInScene:self];

                [self removeChildByName:@"categories"];
            };
            posX += buttonWidth + spacingX * 2;
            [cats addChild:button z:0];
        }
        posX = spacingX + buttonWidth/2;
        posY -= (buttonHeight + spacingY * 2);
    }
    
    CCScrollView *catScroll = [[CCScrollView alloc] initWithContentNode:cats];
    [catScroll setAnchorPoint:ccp(0.0f, 0.0f)];
    [catScroll setPosition:ccp(0, 0)];
    [catScroll setPagingEnabled:YES];
    [catScroll setHorizontalScrollEnabled:NO];
    [self addChild:catScroll z:0 name:@"categories"];
}

- (void)onBackClicked:(id)sender
{
    [GameSupport playSound:BUTTON_CLICK];
    CCTransition* t = [CCTransition transitionFadeWithDuration:0.5f];
    t.outgoingSceneAnimated = YES;
    t.incomingSceneAnimated = YES;
    // back to intro scene with transition
    [[CCDirector sharedDirector] replaceScene:[MainMenuScene scene]
                               withTransition:t];
}


@end
