//
//  GameSupport.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-19.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "cocos2d-ui.h"

typedef enum SOUND_TYPE {
    BUTTON_CLICK,
    CLOCK_TIC,
    CLOCK_TOC,
    CORRECT,
    PASS,
    TIME_OUT,
    START
} SoundType;

@interface GameSupport : NSObject

+ (CCColor *)colorFromHexString:(NSString *)hexString;
+ (int)getRandomNumberBetween:(int)from to:(int)to;
+ (float) getRandomFloatBetween:(float)low and:(float)high;
+ (void) popupAdsInScene:(CCScene*)scene within:(float)minDelay and:(float)maxDelay;
+ (void) playSound:(SoundType) soundType;
+ (void) trackSceneWithName:(NSString*) sceneName;
+ (void) trackEventWithCategory: (NSString*) category action:(NSString*)action label:(NSString*)label;
@end
