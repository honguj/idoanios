//
//  Match.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"
#import "CategoryGame.h"

@interface Match : NSObject

@property (nonatomic) int totalRounds;
@property (atomic, retain) NSMutableArray *players;
@property (nonatomic, retain) CategoryGame *category;

- (id)init;
- (void) addPlayer:(Player *)p;
- (NSMutableArray*) getWinner;

@end
