//
//  HowToPlayScene.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-19.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "HowToPlayScene.h"
#import "MainMenuScene.h"
#import "ConstantManager.h"
#import "GameSupport.h"

@implementation HowToPlayScene

+ (HowToPlayScene *)scene
{
	return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Create a back button
    CCButton *backButton = [CCButton buttonWithTitle:@"Thoát" spriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    backButton.label.fontName = [ConstantManager HEADER_BUTTON_FONT_NAME];
    backButton.label.fontSize = [ConstantManager HEADER_BUTTON_FONT_SIZE];
    backButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    backButton.positionType = CCPositionTypeNormalized;
    backButton.position = ccp([ConstantManager BACK_BUTTON_X], [ConstantManager BACK_BUTTON_Y]); // Top Left of screen
    [backButton setTarget:self selector:@selector(onBackClicked:)];
    [self addChild:backButton z:5];

    
    // Create scroll view
    CCNode *grid = [CCNode node];
    grid.contentSize = CGSizeMake([CCDirector sharedDirector].viewSize.width, [CCDirector sharedDirector].viewSize.height * [ConstantManager TUTORIAL_PAGES]);

    float x = [CCDirector sharedDirector].viewSize.width / 2;
    float y = grid.contentSize.height - [CCDirector sharedDirector].viewSize.height / 2;
    for (int i = 0; i < [ConstantManager TUTORIAL_PAGES]; i++){
        NSString* spriteName = [NSString stringWithFormat:@"Tutorial%d.png",i];
        CCSprite *s = [CCSprite spriteWithImageNamed:spriteName];
        s.position = ccp(x,y);
        [grid addChild:s z:1];
        y -= [CCDirector sharedDirector].viewSize.height;
    }
    
    CCScrollView *gridScroll = [[CCScrollView alloc] initWithContentNode:grid];
    [gridScroll setAnchorPoint:ccp(0.0f, 0.0f)];
    [gridScroll setPosition:ccp(0, 0)];
    [gridScroll setPagingEnabled:NO];
    [gridScroll setHorizontalScrollEnabled:NO];
    [self addChild:gridScroll z:0 name:@"Grid Scroll"];
    
    return self;
}

- (void)onBackClicked:(id)sender
{
    [GameSupport playSound:BUTTON_CLICK];
    CCTransition* t = [CCTransition transitionFadeWithDuration:0.5f];
    t.outgoingSceneAnimated = YES;
    t.incomingSceneAnimated = YES;
    // back to intro scene with transition
    [[CCDirector sharedDirector] replaceScene:[MainMenuScene scene]
                               withTransition:t];
}


@end
