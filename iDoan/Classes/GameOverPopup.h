//
//  GameOverPopup.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-12.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"
#import "cocos2d-ui.h"
#import "ResultScene.h"

@interface GameOverPopup : CCSprite

- (void) loadInScene:(ResultScene*) parent;

@end
