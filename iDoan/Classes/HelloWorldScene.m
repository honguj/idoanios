//
//  HelloWorldScene.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright Tic Toc Prod 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "HelloWorldScene.h"
#import "IntroScene.h"

// -----------------------------------------------------------------------
#pragma mark - HelloWorldScene
// -----------------------------------------------------------------------

@implementation HelloWorldScene
{
    CCSprite *_sprite;
}

// -----------------------------------------------------------------------
#pragma mark - Create & Destroy
// -----------------------------------------------------------------------

+ (HelloWorldScene *)scene
{
    return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Enable touch handling on scene node
    self.userInteractionEnabled = YES;
    
    // Create a colored background
    CCNodeColor *background = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.0f green:0.0f blue:1.0f alpha:1.0f]];
    [self addChild:background];
    
    // Create menu with 3 items
    CCSpriteFrame *imageOff = [CCSpriteFrame frameWithImageNamed:@"button_menu.png"];
    CCSpriteFrame *imageOn = [CCSpriteFrame frameWithImageNamed:@"button_menu.png"];
    
//    button = [CCButton buttonWithTitle:@"" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    
    CCButton* item1 = [CCButton buttonWithTitle:@"How to play" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    item1.block = ^(id sender)
    {
        // Item 1 chosen
        
    };
    
    CCButton* item2 = [CCButton buttonWithTitle:@"Dual mode" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    item2.block = ^(id sender)
    {
        // Item 2 chosen
    };
    
    CCButton* item3 = [CCButton buttonWithTitle:@"Quick play" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    item3.block = ^(id sender)
    {
        // Item 2 chosen
    };
    
    NSArray* menuItems = @[item1, item2, item3];
    
    // Use 	CCLayoutBox to replicate the menu layout feature
    CCLayoutBox* menuBox = [[CCLayoutBox alloc] init];
    
    // Have to set up direction and spacing before adding children
    menuBox.direction = CCLayoutBoxDirectionVertical;
    menuBox.spacing = 30.0f;
    
    menuBox.position = ccp([CCDirector sharedDirector].viewSize.width/2, 150);
    menuBox.anchorPoint = ccp(0.5f, 0.5f);
    
    menuBox.cascadeColorEnabled = YES;
    menuBox.cascadeOpacityEnabled = YES;
    
    // No direct way of adding an array of items (and also need to flip the cascade flags on them all...)
    for (CCNode* item in menuItems)
    {
        item.cascadeColorEnabled = item.cascadeOpacityEnabled = YES;
        [menuBox addChild:item];
    }
    
    // Have to do this *after* all children are added and have the cascade flags set
    menuBox.opacity = 0.0f;
    
    [menuBox runAction:[CCActionFadeIn actionWithDuration:2.0f]];
    
    NSLog(@"Menu size: %.1f, %.1f", menuBox.contentSize.width, menuBox.contentSize.height);
    
    // Add the menu to this layer
    [self addChild:menuBox];
    
//    // Add a sprite
//    _sprite = [CCSprite spriteWithImageNamed:@"Icon-72.png"];
//    _sprite.position  = ccp(self.contentSize.width/2,self.contentSize.height/2);
//    [self addChild:_sprite];
//    
//    // Animate sprite with action
//    CCActionRotateBy* actionSpin = [CCActionRotateBy actionWithDuration:1.5f angle:360];
//    [_sprite runAction:[CCActionRepeatForever actionWithAction:actionSpin]];
//    
    // Create a back button
    CCButton *backButton = [CCButton buttonWithTitle:@"[ Menu ]" fontName:@"Verdana-Bold" fontSize:18.0f];
    backButton.positionType = CCPositionTypeNormalized;
    backButton.position = ccp(0.85f, 0.95f); // Top Right of screen
    [backButton setTarget:self selector:@selector(onBackClicked:)];
    [self addChild:backButton];

    // done
	return self;
}

// -----------------------------------------------------------------------

- (void)dealloc
{
    // clean up code goes here
}

// -----------------------------------------------------------------------
#pragma mark - Enter & Exit
// -----------------------------------------------------------------------

- (void)onEnter
{
    // always call super onEnter first
    [super onEnter];
    
    // In pre-v3, touch enable and scheduleUpdate was called here
    // In v3, touch is enabled by setting userInterActionEnabled for the individual nodes
    // Per frame update is automatically enabled, if update is overridden
    
}

// -----------------------------------------------------------------------

- (void)onExit
{
    // always call super onExit last
    [super onExit];
}

// -----------------------------------------------------------------------
#pragma mark - Touch Handler
// -----------------------------------------------------------------------

-(void) touchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchLoc = [touch locationInNode:self];
    
    // Log touch location
    CCLOG(@"Move sprite to @ %@",NSStringFromCGPoint(touchLoc));
    
    // Move our sprite to touch location
    CCActionMoveTo *actionMove = [CCActionMoveTo actionWithDuration:1.0f position:touchLoc];
    [_sprite runAction:actionMove];
}

// -----------------------------------------------------------------------
#pragma mark - Button Callbacks
// -----------------------------------------------------------------------

- (void)onBackClicked:(id)sender
{
    // back to intro scene with transition
    [[CCDirector sharedDirector] replaceScene:[IntroScene scene]
                               withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionRight duration:1.0f]];
}

// -----------------------------------------------------------------------
@end
