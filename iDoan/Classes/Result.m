//
//  Result.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "Result.h"

@implementation Result

@synthesize guessingWord;
@synthesize isCorrect;

- (id) initWithWord:(NSString *)word correct:(bool)c{
    self = [super init];
    if (!self) return(nil);
    
    guessingWord = word;
    isCorrect = c;
    
    return self;
}

@end
