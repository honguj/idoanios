//
//  ConstantManager.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-17.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface ConstantManager : NSObject

// FONT SIZE RATIO
+ (float)FONT_SIZE_SCALE_RATIO;

// LOADING SCENE
+ (NSString*) TIP_TO_PLAY;

// INTERSTITIAL ADS
+ (float)INTERSTITIAL_ADS_RATIO;

// POPUP CONSTANTS
+ (float)POPUP_TITLE_TEXT_SIZE;
+ (float)POPUP_TEXT_SIZE;
+ (float)POPUP_DUAL_TEXT_SIZE;
+ (float)POPUP_INSTRUCTION_SIZE;
+ (float)POPUP_OPTION_BOX_SIZE;
+ (float)POPUP_HEIGHT;
+ (float)POPUP_WIDTH;
+ (CCSpriteFrame*) POPUP_BUTTON_IMAGE_ON;
+ (CCSpriteFrame*) POPUP_BUTTON_IMAGE_OFF;
+ (CCSpriteFrame*) POPUP_CLOSE_BUTTON_IMAGE_ON;
+ (CCSpriteFrame*) POPUP_CLOSE_BUTTON_IMAGE_OFF;

// VERSUS CONSTANTS
+ (int) TEAMS_MAX;
+ (int) TEAMS_MIN;
+ (int) ROUNDS_MAX;
+ (int) ROUNDS_MIN;

// FONT
+ (NSString*) MAIN_FONT_NAME;

// BUTTON POSITION
+ (float)BACK_BUTTON_X;
+ (float)BACK_BUTTON_Y;
+ (float)NEXT_BUTTON_X;
+ (float)NEXT_BUTTON_Y;
+ (NSString*)HEADER_BUTTON_FONT_NAME;
+ (float)HEADER_BUTTON_FONT_SIZE;
+ (CCSpriteFrame*) HEADER_BUTTON_IMAGE_ON;
+ (CCSpriteFrame*) HEADER_BUTTON_IMAGE_OFF;
+ (NSString*)MENU_BUTTON_FONT_NAME;
+ (float)MENU_BUTTON_FONT_SIZE;
+ (NSString*) BUTTON_STROKE_COLOR_CODE;

// CATEGORY SCENE
+ (int)ROWS_PER_SCREEN;
+ (int)COLUMNS_PER_SCREEN;
+ (float)CATEGORY_HUD_HEIGHT;
+ (NSString*)CAT_BUTTON_FONT_NAME;
+ (float)CAT_BUTTON_FONT_SIZE;
+ (NSString*)CAT_BUTTON_COLOR_CODE;

// HOW TO PLAY SCENE
+ (int)TUTORIAL_PAGES;

// RESULT SCENE
+ (int) RESULTS_PER_PAGE;
+ (float) RESULTS_FONT_SIZE;

// GAME SCENE
+ (int) MAX_COUNT_DOWN_TIMER;
+ (float) GUESSING_WORD_FONT_SIZE;
+ (float) TIMER_FONT_SIZE;

@end
