//
//  ConstantManager.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-17.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "ConstantManager.h"

@implementation ConstantManager

// FONT SIZE RATIO
+ (float)FONT_SIZE_SCALE_RATIO {
    if ([CCDirector sharedDirector].viewSize.width >= 1024){
        // IPAD
        return 2;
    }
    else
        return 1.0;
}

// LOADING SCENE
+ (NSString*) TIP_TO_PLAY {
    NSArray* tips = [NSArray arrayWithObjects:
                     @"Hãy kết nối mạng khi chơi để có từ và chủ đề mới nhất",
                     @"Hãy nhớ xoay xuống là đúng, xoay lên là bỏ qua.",
                     @"Đừng nên tốn quá 10 giây để diễn tả 1 từ.",
                     @"Áp dụng luật 'thử thách' của các chủ đề khi bạn đã thuần thục.",
                     @"Chơi càng đông càng vui!!",
                     @"Có thể chơi ở mọi nơi: cắm trại, giờ ra chơi, quán trà sữa...", nil];

    int r = (int)arc4random() % (tips.count);
    return [tips objectAtIndex:r];
}

// INTERSTITIAL ADS
+ (float)INTERSTITIAL_ADS_RATIO { return 0.4f; }

// POPUP Constants
+ (float)POPUP_TITLE_TEXT_SIZE{ return 30.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (float)POPUP_TEXT_SIZE{ return 20.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (float)POPUP_DUAL_TEXT_SIZE { return 25.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (float)POPUP_INSTRUCTION_SIZE { return 20.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (float)POPUP_OPTION_BOX_SIZE{ return 30.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (float)POPUP_HEIGHT { return 250; }
+ (float)POPUP_WIDTH { return 400; }
+ (CCSpriteFrame*) POPUP_BUTTON_IMAGE_ON { return [CCSpriteFrame frameWithImageNamed:@"PopupButtonClicked.png"]; }
+ (CCSpriteFrame*) POPUP_BUTTON_IMAGE_OFF { return [CCSpriteFrame frameWithImageNamed:@"PopupButtonNormal.png"]; }
+ (CCSpriteFrame*) POPUP_CLOSE_BUTTON_IMAGE_ON { return [CCSpriteFrame frameWithImageNamed:@"PopupCloseButtonClicked.png"]; }
+ (CCSpriteFrame*) POPUP_CLOSE_BUTTON_IMAGE_OFF { return [CCSpriteFrame frameWithImageNamed:@"PopupCloseButtonNormal.png"]; }

// Thi Dau CONSTANTS
+ (int) TEAMS_MAX{ return 4; }
+ (int) TEAMS_MIN { return 2; }
+ (int) ROUNDS_MAX { return 5; }
+ (int) ROUNDS_MIN { return 1; }

// FONT
+ (NSString*) MAIN_FONT_NAME { return @"UVNBanhMi"; }

// BACK BUTTON
+ (float)BACK_BUTTON_X { return 0.08f; }
+ (float)BACK_BUTTON_Y { return 0.93f; }
+ (float)NEXT_BUTTON_X { return 0.93f; }
+ (float)NEXT_BUTTON_Y { return 0.93f; }
+ (NSString*)HEADER_BUTTON_FONT_NAME { return @"UVNBanhMi"; }
+ (float)HEADER_BUTTON_FONT_SIZE { return 15.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (CCSpriteFrame*) HEADER_BUTTON_IMAGE_OFF { return [CCSpriteFrame frameWithImageNamed:@"BackButtonNormal.png"]; }
+ (CCSpriteFrame*) HEADER_BUTTON_IMAGE_ON { return [CCSpriteFrame frameWithImageNamed:@"BackButtonClicked.png"]; }
+ (NSString*)MENU_BUTTON_FONT_NAME { return @"UVNBanhMi"; }
+ (float)MENU_BUTTON_FONT_SIZE { return 20.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (NSString*) BUTTON_STROKE_COLOR_CODE { return @"#5F060E"; }

// CATEGORY SCENE
+ (int)ROWS_PER_SCREEN { return 2; }
+ (int)COLUMNS_PER_SCREEN { return 3; }
+ (float)CATEGORY_HUD_HEIGHT { return 50 * [self FONT_SIZE_SCALE_RATIO]; }
+ (NSString*)CAT_BUTTON_FONT_NAME { return @"UVNBanhMi"; }
+ (float)CAT_BUTTON_FONT_SIZE { return 20.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (NSString*)CAT_BUTTON_COLOR_CODE { return @"#F79020"; }

// HOW TO PLAY SCENE
+ (int)TUTORIAL_PAGES { return 4; }

// RESULT SCENE
+ (int) RESULTS_PER_PAGE { return 6; }
+ (float) RESULTS_FONT_SIZE { return 30.0f * [self FONT_SIZE_SCALE_RATIO]; }

// GAME SCENE
+ (int) MAX_COUNT_DOWN_TIMER {
//#ifdef DEBUG
//    return 10;
//#endif
    return 60;
}
+ (float) GUESSING_WORD_FONT_SIZE { return 45.0f * [self FONT_SIZE_SCALE_RATIO]; }
+ (float) TIMER_FONT_SIZE { return 25.0f * [self FONT_SIZE_SCALE_RATIO]; }

@end
