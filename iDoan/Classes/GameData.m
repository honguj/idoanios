//
//  GameData.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-17.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "GameData.h"

@implementation GameData

@synthesize categories;
@synthesize categoryDict;

static NSString* CategoryKey = @"categories";
static NSString* WordKey = @"words";

+ (id)sharedData {
    static GameData *sharedGameData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedGameData = [self loadInstance];
    });
    return sharedGameData;
}

// Save data
- (void)encodeWithCoder:(NSCoder *)encoder {
//    NSLog(@"Encoder get called");
    [encoder encodeObject:categories   forKey:CategoryKey];
    [encoder encodeObject:categoryDict forKey:WordKey];
}

// Load data
- (id)initWithCoder:(NSCoder *)coder {
    self = [self init];
    NSArray *categoriesDict = [coder decodeObjectForKey:CategoryKey];
    self.categories   = [NSMutableArray arrayWithArray:categoriesDict];
    
    NSDictionary *wordsDict = [coder decodeObjectForKey:WordKey];
    self.categoryDict   = [[NSMutableDictionary alloc] initWithDictionary:wordsDict copyItems:YES];
//    NSLog(@"Load data - words size: %lu", self.categoryDict.count);
    
    return self;
}

+(NSString*)filePath
{
    static NSString* filePath = nil;
    if (!filePath) {
        filePath =
        [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
         stringByAppendingPathComponent:@"gamedata"];
    }
    return filePath;
}

+(instancetype)loadInstance
{
    NSData* decodedData = [NSData dataWithContentsOfFile: [GameData filePath]];
    NSLog(@"loadInstance - filePath = %@", [GameData filePath] );
    if (decodedData) {
        GameData* gameData = [NSKeyedUnarchiver unarchiveObjectWithData:decodedData];
        return gameData;
    }
    return [[GameData alloc] init];
}

-(void)saveCategories:(NSMutableArray*) cats{
    self.categories = cats;
    [self save];
}

-(void)saveWords:(NSMutableArray*) newWords withHash:(NSString*) hash{
    if (![self.categoryDict objectForKey:hash]){ // New hash
        [self.categoryDict setObject:newWords forKey:hash];
    }
    else {
        // Do not insert duplicated words
        NSMutableArray* tmp = [[self.categoryDict objectForKey:hash] mutableCopy];
        for (NSString* w in newWords){
            if (![tmp containsObject:w]){
                [tmp addObject:w];
            }
        }
        [self.categoryDict setObject:tmp forKey:hash];
    }
    [self save];
}

-(void)save
{
    NSData* encodedData = [NSKeyedArchiver archivedDataWithRootObject: self];
    [encodedData writeToFile:[GameData filePath] atomically:YES];
}

@end
