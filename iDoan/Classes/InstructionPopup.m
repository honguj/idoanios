//
//  InstructionPopup.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "InstructionPopup.h"
#import "GameScene.h"
#import "GameManager.h"
#import "ConstantManager.h"
#import "GameSupport.h"


@interface InstructionPopup(){
    CategoriesScene *parent;
    NSString *instruction;
}

@end

@implementation InstructionPopup

- (id)loadInScene:(CategoriesScene *)p {
    // Init class variables
    parent = p;
//    instruction = [[[[GameManager sharedManager] match] category] instruction];
    NSString* description = [[[[GameManager sharedManager] match] category ] description];
    instruction = [NSString stringWithFormat:@"%@ \n %@", description, [[[[GameManager sharedManager] match] category] instruction]];
    NSString *catName = [[[[GameManager sharedManager] match] category] name];
    
    // Create buttons
    CCButton* closeButton = [CCButton buttonWithTitle:@"" spriteFrame:[ConstantManager POPUP_CLOSE_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager POPUP_CLOSE_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    closeButton.positionType = CCPositionTypeNormalized;
    closeButton.position = ccp(0.98f, 0.98f);
    closeButton.block = ^(id sender)
    {
        // close button selected
        [GameSupport playSound:BUTTON_CLICK];
        [parent removeChild:self];
        [parent loadCategoryGrid];
    };
    [self addChild:closeButton z:1 name:@"close"];

    CCButton* playButton = [CCButton buttonWithTitle:@"Bắt đầu" spriteFrame:[ConstantManager POPUP_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager POPUP_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    playButton.label.fontName = [ConstantManager MAIN_FONT_NAME];
    playButton.label.fontSize = [ConstantManager MENU_BUTTON_FONT_SIZE];
    playButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    playButton.positionType = CCPositionTypeNormalized;
    playButton.position = ccp(0.5f, 0.15f);
    playButton.block = ^(id sender)
    {
        // play button selected
        [GameSupport playSound:BUTTON_CLICK];
        [parent removeChild:self];
        
        CCTransition* t = [CCTransition transitionFadeWithDuration:0.1f];
        [[CCDirector sharedDirector] replaceScene:[GameScene scene]
                                    withTransition:t];
    };
    [self addChild:playButton z:1 name:@"play"];
    
    // Title label
    CCLabelTTF *title= [CCLabelTTF labelWithString:catName fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager POPUP_TITLE_TEXT_SIZE]];
    title.color = [CCColor colorWithRed:1.0 green:0 blue:0];
    title.positionType = CCPositionTypeNormalized;
    title.position = ccp(0.5f, 0.9f);
    [self addChild:title z:1 name:@"title"];
    
    // Instruction label
    CCLabelTTF *instructionLabel = [CCLabelTTF labelWithString:instruction fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager POPUP_INSTRUCTION_SIZE]
                                         dimensions:CGSizeMake([CCDirector sharedDirector].viewSize.width * 3 / 4, [CCDirector sharedDirector].viewSize.height * 3 / 4)];
    instructionLabel.positionType = CCPositionTypeNormalized;
    instructionLabel.position = ccp(0.5f, 0.55f);
    instructionLabel.horizontalAlignment = CCTextAlignmentCenter;
    instructionLabel.verticalAlignment = CCTextAlignmentCenter;
    [self addChild:instructionLabel z:1 name:@"instruction"];
    
    return self;
}

@end
