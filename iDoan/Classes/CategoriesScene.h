//
//  CategoriesScene.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface CategoriesScene : CCScene

+ (CategoriesScene *)scene;
- (id)init;
- (void) loadCategoryGrid;
@end
