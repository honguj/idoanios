//
//  Category.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryGame : NSObject <NSCoding>

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* hash;
@property (nonatomic, retain) NSString* description;
@property (nonatomic, retain) NSString* instruction;
@property (nonatomic) bool isLocked;
@property (nonatomic, retain) NSMutableArray* words;

-(id) initWithName:(NSString*)name hash:(NSString*)hash;

@end
