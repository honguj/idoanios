//
//  Match.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "Match.h"

@implementation Match

@synthesize category;
@synthesize totalRounds;
@synthesize players;

- (id)init{
    self = [super init];
    if (!self) return nil;
    
    self.totalRounds = 1;
    
    self.players = [[NSMutableArray alloc] init];
    int numberOfPlayers = 1; // by default
    for (int i = 0 ; i < numberOfPlayers; i++){
        NSString* playerName = [NSString stringWithFormat:@"%@ %d", @"Đội ", i+1];
        Player *p = [[Player alloc] initWithName:playerName];
        [self.players addObject:p];
    }
    NSLog(@"In Match constructor: %lu", (unsigned long)self.players.count);
    return self;
}

- (void) addPlayer:(Player *)p{
    [self.players addObject:p];
}


- (NSMutableArray*) getWinner{
    NSMutableArray* winnerList = [[NSMutableArray alloc] initWithCapacity:self.players];
    Player *winner = nil;
    
    if (self.players.count > 0){
        winner = self.players[0];
        [winnerList addObject:winner];
    }
    else return nil;
    
    for (int i = 1; i < self.players.count; i++){
        Player *p = self.players[i];
        if (p.score == winner.score){
            [winnerList addObject:p];
        }
        if (p.score > winner.score){
            [winnerList removeAllObjects];
            winner = p;
            [winnerList addObject:p];
        }
    }
    
    return winnerList;
}

@end
