//
//  GameOverPopup.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-12.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "GameOverPopup.h"
#import "MainMenuScene.h"
#import "GameManager.h"
#import "ConstantManager.h"
#import "GameSupport.h"

@interface GameOverPopup(){
    ResultScene* parent;
}

@end

@implementation GameOverPopup

- (void) loadInScene:(ResultScene *)p{
    // Init class variables
    parent = p;
    
    // Title label
    CCLabelTTF *title= [CCLabelTTF labelWithString:@"KẾT QUẢ" fontName:[ConstantManager HEADER_BUTTON_FONT_NAME] fontSize:35.0f];
    title.color = [CCColor colorWithRed:1.0f green:0.0f blue:0.0f];
    title.positionType = CCPositionTypeNormalized;
    title.position = ccp(0.5f, 0.9f);
    [self addChild:title z:0 name:@"title"];
    
    // Create buttons
    CCSpriteFrame *imageOff = [CCSpriteFrame frameWithImageNamed:@"DualResultButtonNormal.png"];
    CCSpriteFrame *imageOn = [CCSpriteFrame frameWithImageNamed:@"DualResultButtonClicked.png"];
    
    CCButton* backToMenuButton = [CCButton buttonWithTitle:@"Thoát" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    backToMenuButton.label.fontName = [ConstantManager MAIN_FONT_NAME];
    backToMenuButton.label.fontSize = [ConstantManager MENU_BUTTON_FONT_SIZE];
    backToMenuButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    backToMenuButton.positionType = CCPositionTypeNormalized;
    backToMenuButton.color = [CCColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f];
    backToMenuButton.position = ccp(0.5f, 0.15f);
    backToMenuButton.block = ^(id sender)
    {
        [GameSupport playSound:BUTTON_CLICK];
        [parent removeChild:self];
        [[GameManager sharedManager] resetMatch];
        CCTransition* t = [CCTransition transitionFadeWithDuration:0.1f];
        [[CCDirector sharedDirector] replaceScene:[MainMenuScene scene]
                                   withTransition:t];
    };
    [self addChild:backToMenuButton z:1 name:@"backToMenu"];
    
    [self loadResultGrid];
    
    // Load winner label
    NSMutableArray* winners = [[[GameManager sharedManager] match] getWinner];
    NSString* winnerText = @"";
    if (winners.count > 1){
        // Many winners
        for ( Player* p in winners){
            winnerText = [winnerText stringByAppendingString:[NSString stringWithFormat:@"%@, ", p.name]];
        }
        winnerText = [winnerText substringToIndex:winnerText.length - 2];
        winnerText = [winnerText stringByAppendingString:@" cùng thắng!"];
    }
    else {
        winnerText = [NSString stringWithFormat:@"%@ thắng!", ((Player*)[winners objectAtIndex:0]).name];
    }
    CCLabelTTF *winnerLabel= [CCLabelTTF labelWithString:winnerText fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager POPUP_TITLE_TEXT_SIZE]];
    winnerLabel.positionType = CCPositionTypeNormalized;
    winnerLabel.position = ccp(0.5f, 0.35f);
    winnerLabel.color = [CCColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    [self addChild:winnerLabel z:0 name:@"winner"];
}

- (void) loadResultGrid{
    
    CCNode *grid = [CCNode node];
    grid.contentSize = CGSizeMake([CCDirector sharedDirector].viewSize.width, [CCDirector sharedDirector].viewSize.height);
    
    int x = [CCDirector sharedDirector].viewSize.width/3;
    int y = grid.contentSize.height - 2 * [ConstantManager POPUP_TITLE_TEXT_SIZE];
    for (int i = 0; i < [[[[GameManager sharedManager] match] players]count]; i++) {
        Player* p = [[[GameManager sharedManager] match] players][i];
        NSString* text = [NSString stringWithFormat:@"%@ : %d điểm",  p.name, p.score];
        CCLabelTTF *result = [CCLabelTTF labelWithString:text fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager POPUP_TEXT_SIZE]];
        result.position = ccp(x,y);
        [grid addChild:result];
        y -= [ConstantManager POPUP_TEXT_SIZE];
    }
    
    CCScrollView *gridScroll = [[CCScrollView alloc] initWithContentNode:grid];
    [gridScroll setAnchorPoint:ccp(0.0f, 0.0f)];
    [gridScroll setPosition:ccp(0, 0)];
    [gridScroll setPagingEnabled:NO];
    [gridScroll setHorizontalScrollEnabled:NO];
    [gridScroll setVerticalScrollEnabled:NO];
    [self addChild:gridScroll z:0 name:@"Grid Scroll"];
}


@end
