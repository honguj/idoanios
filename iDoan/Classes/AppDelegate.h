//
//  AppDelegate.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright Tic Toc Prod 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "GADInterstitial.h"

#define ADMOB_BANNER_UNIT_ID @"ca-app-pub-4120692219431963/2928310135";
#define ADMOB_POPUP_UNIT_ID @"ca-app-pub-4120692219431963/8951053739";
#define GOOGLE_ANALYTIC_TRACKING_ID @"UA-53372606-3";

@interface AppDelegate : CCAppDelegate {

GADInterstitial* interstitial_;

}

- (void) showInterstitial;

typedef enum _bannerType
{
    kBanner_Portrait_Top,
    kBanner_Portrait_Bottom,
    kBanner_Landscape_Top,
    kBanner_Landscape_Bottom,
}CocosBannerType;

#define BANNER_TYPE kBanner_Landscape_Bottom

@end
