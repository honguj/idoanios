//
//  InstructionPopup.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CCSprite.h"
#import "CategoriesScene.h"
#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface InstructionPopup : CCSprite

- (id)loadInScene:(CategoriesScene *)p;

@end
