//
//  Player.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "Player.h"

@implementation Player

-(id)initWithName:(NSString *)name{
    self = [super init];
    if (!self) return nil;
    
    self.name = name;
    self.score = 0;
    self.results = [[NSMutableArray alloc] init];
    
    return self;
}

-(void)addResult:(Result *)r{
    [self.results addObject:r];
}

-(void)resetResult{
    [self.results removeAllObjects];
}

@end
