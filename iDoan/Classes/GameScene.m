//
//  GameScene.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "GameScene.h"
#import "CategoriesScene.h"
#import "ResultScene.h"
#import "GameManager.h"
#import "GameSupport.h"
#import "ConstantManager.h"
#import <AudioToolbox/AudioToolbox.h>

@interface GameScene(){
    int countdown;
    int readyCountDown;
    CCLabelTTF *timerText;
    CCLabelTTF *guessingWord;
    CCSprite *background;
    NSMutableArray *words;
    bool isStarted;
    bool isWaitingState;
}

@end

@implementation GameScene

@synthesize motionManager;

+ (GameScene *)scene
{
	return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Enable touch handling on scene node
    self.userInteractionEnabled = YES;
    
    // Init class attributes
    isStarted = false;
    readyCountDown = 3;
    countdown = [ConstantManager MAX_COUNT_DOWN_TIMER];
    words = [[[[GameManager sharedManager] match] category] words];
    isWaitingState = false;
    
    // Create a colored background
    background = [CCSprite spriteWithImageNamed:@"GameSceneBackground.png"];
    background.position  = ccp(self.contentSize.width/2,self.contentSize.height/2);
    background.name = @"background";
    [self addChild:background];

    
    // Timer text
    timerText = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", [ConstantManager MAX_COUNT_DOWN_TIMER]] fontName:@"Arial" fontSize:[ConstantManager TIMER_FONT_SIZE]];
    timerText.positionType = CCPositionTypeNormalized;
    timerText.position = ccp(0.5f, 0.95);
    [self addChild:timerText];
    
    // Guessing word
    NSString* s = @"Chạm để bắt đầu chơi";
    if ([[[[GameManager sharedManager] match] players] count] > 1) {// Dual mode
        s = [NSString stringWithFormat:@"%@ - Vòng %d \n Chạm để bắt đầu chơi", [[[GameManager sharedManager] getCurrentPlayer] name], [[GameManager sharedManager] currentRound] + 1];
    }
    guessingWord = [CCLabelTTF labelWithString:s fontName:@"Arial Rounded MT Bold" fontSize:[ConstantManager GUESSING_WORD_FONT_SIZE]];
    guessingWord.dimensions = CGSizeMake([CCDirector sharedDirector].viewSize.width, [CCDirector sharedDirector].viewSize.height * 3 / 4);
    guessingWord.positionType = CCPositionTypeNormalized;
    guessingWord.position = ccp(0.5f, 0.5f); // Middle of screen
    guessingWord.horizontalAlignment = CCTextAlignmentCenter;
    guessingWord.verticalAlignment = CCTextAlignmentCenter;
    [self addChild:guessingWord];
    
    // Create a back button
    CCButton *backButton = [CCButton buttonWithTitle:@"Thoát" spriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    backButton.label.fontName = [ConstantManager HEADER_BUTTON_FONT_NAME];
    backButton.label.fontSize = [ConstantManager HEADER_BUTTON_FONT_SIZE];
    backButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    backButton.positionType = CCPositionTypeNormalized;
    backButton.position = ccp([ConstantManager BACK_BUTTON_X], [ConstantManager BACK_BUTTON_Y]); // Top Right of screen
    [backButton setTarget:self selector:@selector(onBackClicked:)];
    [self addChild:backButton];
    
    // Motion manager
    self.motionManager = [[CMMotionManager alloc] init];
    motionManager.deviceMotionUpdateInterval = 1.0/60.0;
    if (motionManager.isDeviceMotionAvailable) {
        [motionManager startDeviceMotionUpdates];
    }
    
    // Play start sound
    [GameSupport playSound:START];
    
    // Google analytics
    [GameSupport trackEventWithCategory:@"GameScene" action:@"Play" label:[[[[GameManager sharedManager] match] category] name]];
    
    // Test label - TO REMOVE
    // add and position the labels
//    yawLabel = [CCLabelTTF labelWithString:@"Yaw: " fontName:@"Marker Felt" fontSize:12];
//    yawLabel.position =  ccp(100, 240);
//    [self addChild: yawLabel];
    
    return self;
}

- (void)update:(CCTime)delta {
    CMDeviceMotion *currentDeviceMotion = motionManager.deviceMotion;
    CMAttitude *currentAttitude = currentDeviceMotion.attitude;
    
    // 1: Convert the radians yaw value to degrees then round up/down
    rollAngle = roundf((float)(CC_RADIANS_TO_DEGREES(currentAttitude.roll)));
    float pitch = roundf((float)(CC_RADIANS_TO_DEGREES(currentAttitude.pitch)));
    float yaw = roundf((float)(CC_RADIANS_TO_DEGREES(currentAttitude.yaw)));
    rollAngle = abs(rollAngle);
    // 2: Convert the degrees value to float and use Math function to round the value
    [yawLabel setString:[NSString stringWithFormat:@"Yaw: %.0f - Pitch: %.0f - Roll: %.0f", yaw, pitch, rollAngle]];

    // exit if the game is not started
    if (!isStarted) return;
    
    // Check whether user has correct answer
    if (isWaitingState){
        if ( rollAngle > 80 && rollAngle < 100){
            background = [CCSprite spriteWithImageNamed:@"GameSceneBackground.png"];
            [self changeBackgroundWithColor:background];
            [self generateNewWord];
            isWaitingState = false;
        }
    } else {
        if (rollAngle < 20){
            // Fail
            [GameSupport playSound:PASS];
            [[[GameManager sharedManager] getCurrentPlayer] addResult:[[Result alloc] initWithWord:guessingWord.string correct:false]];
            background = [CCSprite spriteWithImageNamed:@"PassBackground.png"];
            [self changeBackgroundWithColor:background];
            guessingWord.string = @"BỎ QUA!";
            isWaitingState = true;
        } else if (rollAngle > 160){
            // Correct
            [GameSupport playSound:CORRECT];
            [[[GameManager sharedManager] getCurrentPlayer] addResult:[[Result alloc] initWithWord:guessingWord.string correct:true]];
            background = [CCSprite spriteWithImageNamed:@"SuccessBackground.png"];
            [self changeBackgroundWithColor:background];
            guessingWord.string = @"ĐÚNG RỒI!";
            isWaitingState = true;
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }
        
    }
}

- (void) countDown {
    if (countdown <= 0){
        [self unschedule:@selector(countDown)];
        self.userInteractionEnabled = NO;
        isStarted = false;
        [self gameOver];
        return;
    }
    countdown--;
    [timerText setString:[NSString stringWithFormat:@"%i", countdown]];
}

- (void) readyCountDown {
    if (readyCountDown <=0){
        if (readyCountDown ==0)
            [GameSupport playSound:CLOCK_TOC];
        
        [self unschedule:@selector(readyCountDown)];
        
        guessingWord.color = [CCColor colorWithRed:1 green:1 blue:1];
        // Start countdown
        [self schedule:@selector(countDown) interval:1.0f];
        // generate new word
        [self generateNewWord];
        // init roll angle
        rollAngle = 90;
        isStarted = true;
    }
    else {
        [GameSupport playSound:CLOCK_TIC];
        guessingWord.string = [NSString stringWithFormat:@"%i", readyCountDown];
        guessingWord.color = [CCColor colorWithRed:1.0 green:0 blue:0];
        readyCountDown--;
    }
}

- (void) generateNewWord {
    int r = (int)arc4random() % (words.count);
//    NSLog(@"GenerateWord - rand= %d", r);
    guessingWord.string = words[r];
//    NSLog(@"guessingWord len = %f", guessingWord.contentSize.width );
    [words removeObjectAtIndex:r];
}

- (void) changeBackgroundWithColor: (CCSprite*)color{
    [self removeChildByName:@"background"];
    color.name = @"background";
    color.position  = ccp(self.contentSize.width/2,self.contentSize.height/2);
    [self addChild:color z:-10];
}

- (void) gameOver{
    if (! [guessingWord.string isEqualToString:@"BỎ QUA!"]){
        [[[GameManager sharedManager] getCurrentPlayer] addResult:[[Result alloc] initWithWord:guessingWord.string correct:false]];
    }
    
    CCActionCallBlock *showTimesUp = [CCActionCallBlock actionWithBlock:^{
        [GameSupport playSound:TIME_OUT];
        background = [CCSprite spriteWithImageNamed:@"PassBackground.png"];
        [self changeBackgroundWithColor:background];
        guessingWord.string = @"HẾT GIỜ!";
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }];
    CCActionDelay *delayAction = [CCActionDelay actionWithDuration:2.0];
    CCActionCallBlock *switchSceneAction = [CCActionCallBlock actionWithBlock:^{
        CCTransition* t = [CCTransition transitionFadeWithDuration:0.5f];
        t.outgoingSceneAnimated = YES;
        t.incomingSceneAnimated = YES;
        // back to intro scene with transition
        [[CCDirector sharedDirector] replaceScene:[ResultScene scene]
                                   withTransition:t];
    }];

    [self runAction:[CCActionSequence actions:showTimesUp, delayAction, switchSceneAction, nil]];
}

- (void)onBackClicked:(id)sender
{
    [GameSupport playSound:BUTTON_CLICK];
    CCTransition* t = [CCTransition transitionFadeWithDuration:0.5f];
    t.outgoingSceneAnimated = YES;
    t.incomingSceneAnimated = YES;
    // back to intro scene with transition
    [[CCDirector sharedDirector] replaceScene:[CategoriesScene scene]
                               withTransition:t];
}

// -----------------------------------------------------------------------
#pragma mark - Touch Handler
// -----------------------------------------------------------------------

-(void) touchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    if (!isStarted){
        // Ready countdown
        [self schedule:@selector(readyCountDown) interval:1.0f];
    }
}

@end
