//
//  GameManager.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Match.h"

@interface GameManager : NSObject

@property (nonatomic, retain) Match *match;
@property (nonatomic, retain) NSMutableArray *categories;
@property (nonatomic) int currentRound;
@property (nonatomic) int currentPlayer;

+ (id)sharedManager;
- (void) loadAllCategories;
- (void) loadCategoryWords:(NSString *)hash;
- (bool) gameIsDone;
- (Player*) getCurrentPlayer;
- (void) nextPlayer;
- (void) nextRound;
- (void) setNumberOfRounds:(int) rounds;
- (void) setNumberOfTeams:(int) numberOfTeams;
- (void) resetMatch;
- (bool) roundEnded;
- (bool) gameOver;
@end

