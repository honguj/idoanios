//
//  CategoryParser.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-08.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryParser : NSObject <NSXMLParserDelegate>
{
	NSMutableArray	*words;
	NSXMLParser		*parser;
}

@property (readonly, retain) NSMutableArray	*words;

-(id) loadXMLByURL:(NSString *)urlString;
-(id) loadXMLLocal:(NSString *)fileName;

@end
