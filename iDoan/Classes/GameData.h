//
//  GameData.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-17.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameData : NSObject <NSCoding>

@property (nonatomic, strong) NSMutableArray *categories;
@property (nonatomic, strong) NSMutableDictionary *categoryDict;

+ (id)sharedData;
-(void)saveCategories:(NSMutableArray*) cats;
-(void)saveWords:(NSMutableArray*) newWords withHash:(NSString*) hash;
-(void)save;

@end
