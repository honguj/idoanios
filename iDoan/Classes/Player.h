//
//  Player.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Result.h"

@interface Player : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic) int score;
@property (atomic, retain) NSMutableArray *results;

-(id)initWithName:(NSString *) name;
-(void)addResult:(Result *)r;
-(void)resetResult;

@end
