//
//  MainMenuScene.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "MainMenuScene.h"
#import "CategoriesScene.h"
#import "HowToPlayScene.h"

#import "InstructionPopup.h"
#import "DualModePopup.h"
#import "LoadingPopup.h"

#import "GameManager.h"
#import "ConstantManager.h"

#import "AppDelegate.h"
#import "GameSupport.h"

@interface MainMenuScene(){

}

@end

@implementation MainMenuScene

// -----------------------------------------------------------------------
#pragma mark - Create & Destroy
// -----------------------------------------------------------------------

+ (MainMenuScene *)scene
{
	return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Create a colored background
    CCSprite *background = [CCSprite spriteWithImageNamed:@"MenuBackground.png"];
    background.position  = ccp(self.contentSize.width/2,self.contentSize.height/2);
    [self addChild:background];

//    self.userInteractionEnabled = YES;
    
    [self createMenu];
    
    // Popup ads
    [GameSupport popupAdsInScene:self within:1.0f and:2.5f];
    
    // Google analytics
    [GameSupport trackSceneWithName:@"MainMenuScene"];
    
    return self;
}

- (void) createMenu{
    // Create menu with 3 items
    CCSpriteFrame *imageOff = [CCSpriteFrame frameWithImageNamed:@"MenuButtonNormal.png"];
    CCSpriteFrame *imageOn = [CCSpriteFrame frameWithImageNamed:@"MenuButtonClicked.png"];
    
    CCButton* item1 = [CCButton buttonWithTitle:@"CÁCH CHƠI" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    item1.label.fontName = [ConstantManager MENU_BUTTON_FONT_NAME];
    item1.label.fontSize = [ConstantManager MENU_BUTTON_FONT_SIZE];
    item1.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    item1.block = ^(id sender)
    {
        // Item 1 chosen
        [GameSupport playSound:BUTTON_CLICK];
        CCTransition* t = [CCTransition transitionFadeWithDuration:0.1f];
        [[CCDirector sharedDirector] replaceScene:[HowToPlayScene scene]
                                   withTransition:t];
        
    };
    
    CCButton* item2 = [CCButton buttonWithTitle:@"THI ĐẤU" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    item2.label.fontName = [ConstantManager MENU_BUTTON_FONT_NAME];
    item2.label.fontSize = [ConstantManager MENU_BUTTON_FONT_SIZE];
    item2.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    item2.block = ^(id sender)
    {
        // Item 2 chosen
        [GameSupport playSound:BUTTON_CLICK];
        DualModePopup* popup = [DualModePopup spriteWithImageNamed:@"PopupBackground.png"];
        [self addChild:popup z:2];
        popup.positionType = CCPositionTypeNormalized;
        popup.position = ccp(0.5f, 0.55f);
        [popup loadInScene:self];
       
    };
    
    CCButton* item3 = [CCButton buttonWithTitle:@"CHƠI NHANH" spriteFrame:imageOff highlightedSpriteFrame:imageOn disabledSpriteFrame:nil];
    item3.label.fontName = [ConstantManager MENU_BUTTON_FONT_NAME];
    item3.label.fontSize = [ConstantManager MENU_BUTTON_FONT_SIZE];
    item3.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    item3.block = ^(id sender)
    {
        // Item 3 chosen
        [GameSupport playSound:BUTTON_CLICK];
        [[GameManager sharedManager] resetMatch];
        
        CCTransition* t = [CCTransition transitionFadeWithDuration:0.1f];
        [[CCDirector sharedDirector] replaceScene:[CategoriesScene scene]
                                   withTransition:t];
    };
    
    NSArray* menuItems = @[item3, item2, item1];
    
    // Use 	CCLayoutBox to replicate the menu layout feature
    CCLayoutBox* menuBox = [[CCLayoutBox alloc] init];
    
    // Have to set up direction and spacing before adding children
    menuBox.direction = CCLayoutBoxDirectionHorizontal; // CCLayoutBoxDirectionVertical;
    menuBox.spacing = 5.0f;
    
    
    menuBox.position = ccp([CCDirector sharedDirector].viewSize.width/2, [CCDirector sharedDirector].viewSize.height/4);
    menuBox.anchorPoint = ccp(0.5f, 0.5f);
    
    menuBox.cascadeColorEnabled = YES;
    menuBox.cascadeOpacityEnabled = YES;
    
    // No direct way of adding an array of items (and also need to flip the cascade flags on them all...)
    for (CCNode* item in menuItems)
    {
        item.cascadeColorEnabled = item.cascadeOpacityEnabled = YES;
        [menuBox addChild:item];
    }
    
    // Have to do this *after* all children are added and have the cascade flags set
    menuBox.opacity = 0.0f;
    
    [menuBox runAction:[CCActionFadeIn actionWithDuration:2.0f]];
    
    NSLog(@"Menu size: %.1f, %.1f", menuBox.contentSize.width, menuBox.contentSize.height);
    
    // Add the menu to this layer
    [self addChild:menuBox z:1 name:@"menu"];
}

@end
