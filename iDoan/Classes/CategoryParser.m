//
//  CategoryParser.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-08.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CategoryParser.h"

@implementation CategoryParser
@synthesize words;

-(id) loadXMLByURL:(NSString *)urlString
{
	words			= [[NSMutableArray alloc] init];
	NSURL *url		= [NSURL URLWithString:urlString];
	NSData	*data   = [[NSData alloc] initWithContentsOfURL:url];
	parser			= [[NSXMLParser alloc] initWithData:data];
	parser.delegate = self;
	[parser parse];
	return self;
}

-(id) loadXMLLocal:(NSString *)fileName
{
    words			= [[NSMutableArray alloc] init];
	
    // Read data from XML
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"xml"];
    
    // Create NSData instance from xml in filePath
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];

	parser			= [[NSXMLParser alloc] initWithData:xmlData];
	parser.delegate = self;
	[parser parse];
	return self;

}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if ([elementname isEqualToString:@"word"])
	{
        NSString* word = [attributeDict objectForKey:@"name"];
        [self.words addObject:word];
	}
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{

}

- (void) dealloc
{

}

@end
