//
//  GameSupport.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-19.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "GameSupport.h"
#import "AppDelegate.h"
#import "ConstantManager.h"
#import "GAITrackedViewController.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "GAI.h"

#define ARC4RANDOM_MAX      0x100000000

@implementation GameSupport

// Assumes input like "#00FF00" (#RRGGBB).
+ (CCColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [CCColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (int)getRandomNumberBetween:(int)from to:(int)to {
    return (int)from + arc4random() % (to-from+1);
}

+(float) getRandomFloatBetween:(float)low and:(float)high
{
    float diff = high - low;
    return (((float) rand() / RAND_MAX) * diff) + low;
}

+ (void)popupAdsInScene:(CCScene*)scene within:(float)minDelay and:(float)maxDelay{
    // Popup ads
    float rand = ((float)arc4random() / ARC4RANDOM_MAX);
    if (rand <= [ConstantManager INTERSTITIAL_ADS_RATIO]){
        float randomDuration = [self getRandomFloatBetween:minDelay and:maxDelay];
//        NSLog(@"POP ADS %f", randomDuration);
        CCActionDelay *delayAction = [CCActionDelay actionWithDuration:randomDuration];
        CCActionCallBlock *showAdsAction = [CCActionCallBlock actionWithBlock:^{
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] showInterstitial];
        }];
        id sequence = [CCActionSequence actions:delayAction, showAdsAction, nil];
        [scene runAction:sequence];
    }
}

+ (void) playSound:(SoundType) type{
    NSString* soundName = nil;
    
    switch (type) {
        case BUTTON_CLICK:
            soundName = @"Click.wav";
            break;
        case CLOCK_TIC:
            soundName = @"Tic.wav";
            break;
        case CLOCK_TOC:
            soundName = @"Toc.wav";
            break;
        case CORRECT:
            soundName = @"Correct.wav";
            break;
        case PASS:
            soundName = @"Pass.wav";
            break;
        case TIME_OUT:
            soundName = @"Ending.wav";
            break;
        case START:
            soundName = @"Start.wav";
            break;
        default:
            return;
    }
    // access audio object
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    // play sound effect
    [audio playEffect:soundName];
}

+ (void) trackSceneWithName:(NSString*) sceneName{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:sceneName];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

+ (void) trackEventWithCategory: (NSString*)category action:(NSString*)action label:(NSString*)label {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category     // Event category (required)
                                                          action:action  // Event action (required)
                                                           label:label          // Event label
                                                           value:nil] build]];
}

@end
