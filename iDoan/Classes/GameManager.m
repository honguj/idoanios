//
//  GameManager.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-06.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "GameManager.h"
#import "GameData.h"
#import "CategoryParser.h"
#import "CategoriesParser.h"
#import "Reachability.h"

//@interface GameManager(){
//    
//}
//
//@end

@implementation GameManager

@synthesize match;
@synthesize categories;
@synthesize currentRound;

+ (id)sharedManager {
    static GameManager *sharedGameManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedGameManager = [[self alloc] init];
    });
    return sharedGameManager;
}

- (id)init {
    if (self = [super init]) {
        // Init variables here
        self.match = [[Match alloc] init];
        self.currentRound = 0;
        self.currentPlayer = 0;
    }
    return self;
}

- (bool) gameIsDone{
    return self.currentRound > self.match.totalRounds;
}

- (Player*) getCurrentPlayer{
    return self.match.players[self.currentPlayer];
}

- (void) nextPlayer{
    self.currentPlayer++;
}

- (void) setNumberOfTeams:(int)numberOfTeams{
    [self.match.players removeAllObjects];
    for(int i = 0; i < numberOfTeams; i++){
        [self.match addPlayer:[[Player alloc] initWithName:[NSString stringWithFormat:@"Đội %d", (i+1)]]];
    }
}

- (void) nextRound {
    self.currentRound++;
    self.currentPlayer = 0;
}

- (void) setNumberOfRounds:(int) rounds{
    self.match.totalRounds = rounds;
}

- (BOOL)checkInternetConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

- (void) loadAllCategories{
    if([self checkInternetConnected] )
    {
        // Load online
        CategoriesParser* categoriesParser = [[CategoriesParser alloc] loadXMLByURL:@"http://tictocprod-idoan.herokuapp.com/clients.xml"];
        self.categories = categoriesParser.categories;
        
        // Check if there is any new categories
        for (CategoryGame* cat in self.categories){
            if (![[[GameData sharedData] categoryDict] objectForKey: cat.hash]){
                // If it is a new category which doesn't exist in the disc -> load its word list online
                NSString* catURL = [NSString stringWithFormat:@"http://tictocprod-idoan.herokuapp.com/clients/%@.xml", cat.hash];
                NSMutableArray* newWordList = [[[CategoryParser alloc] loadXMLByURL:catURL] words];
                [[GameData sharedData] saveWords:newWordList withHash:cat.hash];
            }
        }
        
        // Save new data to disc
        [[GameData sharedData] saveCategories:self.categories];
    }
    else { // Offline
        self.categories = [[GameData sharedData] categories];
//        NSLog(@"Test GameData - cats: %lu", (unsigned long)self.categories.count);
        
        if (self.categories.count == 0){ // In case the data in disc is empty, load default files
            CategoriesParser* categoriesParser = [[CategoriesParser alloc] loadXMLLocal:@"Categories"];
            self.categories = categoriesParser.categories;
        }
    }
 }

- (void) loadCategoryWords:(NSString *)hash{
    
    if ([self checkInternetConnected])
    {
        // Load words list online
        NSString* catURL = [NSString stringWithFormat:@"http://tictocprod-idoan.herokuapp.com/clients/%@.xml", hash];
        self.match.category.words = [[[CategoryParser alloc] loadXMLByURL:catURL] words];
        [[GameData sharedData] saveWords:self.match.category.words withHash:hash];
    }
    else {
        // Check if there is data on the current device
        self.match.category.words = [NSMutableArray arrayWithArray:[[[GameData sharedData] categoryDict] objectForKey:hash]];
//        NSLog(@"GameManager - loadCategoryWords off size: %lu", self.match.category.words.count);
        
        for (NSString* w in self.match.category.words){
            NSLog(@"Test word list: %@", w);
        }
        
        if ( self.match.category.words.count == 0) // There is no data in the current device
            self.match.category.words = [[[CategoryParser alloc] loadXMLLocal:self.match.category.hash] words];
    }
}

- (void) resetMatch{
    self.match = [[Match alloc] init];
    self.currentRound = 0;
    self.currentPlayer = 0;
}

- (bool) roundEnded{
    return self.currentPlayer == self.match.players.count-1;
}

- (bool) gameOver{
    return [self roundEnded] && self.currentRound == self.match.totalRounds-1;
}

@end
