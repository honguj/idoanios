//
//  HowToPlayScene.h
//  iDoan
//
//  Created by Duc Nguyen on 2014-07-19.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "CCScene.h"
#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface HowToPlayScene : CCScene

+ (HowToPlayScene *)scene;
- (id)init;
@end
