//
//  ResultScene.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-05.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "ResultScene.h"
#import "MainMenuScene.h"
#import "GameScene.h"
#import "GameManager.h"
#import "ConstantManager.h"
#import "CategoriesScene.h"
#import "GameOverPopup.h"
#import "AppDelegate.h"
#import "GameSupport.h"

@interface ResultScene(){
    NSArray* results;
    int score;
}

@end

@implementation ResultScene

//static float hudHeight = 80;
static float resultSpacing = 40.0f;

+ (ResultScene *)scene
{
	return [[self alloc] init];
}

// -----------------------------------------------------------------------

- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Init class varialbes
    score = 0;
    results = [[[GameManager sharedManager] getCurrentPlayer] results];
    
    // Create a colored background
    CCSprite *background = [CCSprite spriteWithImageNamed:@"ResultBackground.png"];
    background.position  = ccp(self.contentSize.width/2,self.contentSize.height/2);
    [self addChild:background];
    
    [self loadResultGrid];
    [self loadHUD];
    
    // Popup ads
    [GameSupport popupAdsInScene:self within:1.0f and:3.0f];
    
    // Google analytics
    [GameSupport trackSceneWithName:@"ResultScene"];
    
    return self;
}

- (void) loadHUD{
    CCSprite* resultBox = [CCSprite spriteWithImageNamed:@"ResultBox.png"];
    resultBox.positionType = CCPositionTypeNormalized;
    resultBox.position = ccp(0.5f, 0.9f);
    [self addChild:resultBox z:1 name:@"resultBox"];
    
    CCLabelTTF *scoreText = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i", score] fontName:@"Arial" fontSize:[ConstantManager RESULTS_FONT_SIZE]];
    scoreText.color = [CCColor colorWithRed:0.0f green:0.0f blue:1.0f];
    scoreText.positionType = CCPositionTypeNormalized;
    scoreText.position = ccp(0.5f, 0.9f);
    [self addChild:scoreText z:2 name:@"score"];
    
    if ([[[[GameManager sharedManager] match] players] count] > 1) { // Dual mode
        [self loadButtonsDualMode];
    }
    else {
        [self loadButtonsQuickPlay];
    }
}

- (void) loadButtonsDualMode{
    // Create a next button
    CCButton *nextButton = [CCButton buttonWithTitle:@"Chơi tiếp" spriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    nextButton.label.fontName = [ConstantManager HEADER_BUTTON_FONT_NAME];
    nextButton.label.fontSize = [ConstantManager HEADER_BUTTON_FONT_SIZE];
    nextButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    nextButton.positionType = CCPositionTypeNormalized;
    nextButton.position = ccp([ConstantManager NEXT_BUTTON_X], [ConstantManager NEXT_BUTTON_Y]); // Top Left of screen
    [nextButton setTarget:self selector:@selector(onNextClicked)];
    [self addChild:nextButton z:2 name:@"nextButton"];
    
    // Create a back button
    CCButton *backButton = [CCButton buttonWithTitle:@"Thoát" spriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    backButton.label.fontName = [ConstantManager HEADER_BUTTON_FONT_NAME];
    backButton.label.fontSize = [ConstantManager HEADER_BUTTON_FONT_SIZE];
    backButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    backButton.positionType = CCPositionTypeNormalized;
    backButton.position = ccp([ConstantManager BACK_BUTTON_X], [ConstantManager BACK_BUTTON_Y]); // Top Right of screen
    [backButton setTarget:self selector:@selector(onBackClicked:)];
    [self addChild:backButton z:2 name:@"backButton"];
}

- (void) loadButtonsQuickPlay{
    // Create a next button
    CCButton *nextButton = [CCButton buttonWithTitle:@"Chơi tiếp" spriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_OFF] highlightedSpriteFrame:[ConstantManager HEADER_BUTTON_IMAGE_ON] disabledSpriteFrame:nil];
    nextButton.label.fontName = [ConstantManager HEADER_BUTTON_FONT_NAME];
    nextButton.label.fontSize = [ConstantManager HEADER_BUTTON_FONT_SIZE];
    nextButton.label.outlineColor = [GameSupport colorFromHexString:[ConstantManager BUTTON_STROKE_COLOR_CODE]];
    nextButton.positionType = CCPositionTypeNormalized;
    nextButton.position = ccp([ConstantManager NEXT_BUTTON_X], [ConstantManager NEXT_BUTTON_Y]); // Top Left of screen
    [nextButton setTarget:self selector:@selector(onBackClicked:)];
    [self addChild:nextButton z:2];
}

- (void) loadResultGrid{
    float gridHeight = results.count * (resultSpacing * [ConstantManager FONT_SIZE_SCALE_RATIO] + 10.0f);
    
    CCNode *grid = [CCNode node];
    grid.contentSize = CGSizeMake([CCDirector sharedDirector].viewSize.width, gridHeight);
    int x = [CCDirector sharedDirector].viewSize.width/2;
    int y = grid.contentSize.height - [CCDirector sharedDirector].viewSize.height/4; //hudHeight
    for (int i = 0; i < results.count; i++) {
        CCLabelTTF *result = [CCLabelTTF labelWithString:[results[i] guessingWord] fontName:[ConstantManager MAIN_FONT_NAME] fontSize:[ConstantManager RESULTS_FONT_SIZE]];
        result.position = ccp(x,y);
        result.dimensions = CGSizeMake([CCDirector sharedDirector].viewSize.width, [ConstantManager RESULTS_FONT_SIZE]*2.5);
        result.horizontalAlignment = CCTextAlignmentCenter;
        result.verticalAlignment = CCTextAlignmentCenter;
        if ([results[i] isCorrect]){
            score++;
            result.color = [CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        }
        else
            result.color = [CCColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
        [grid addChild:result];
        y -= resultSpacing * [ConstantManager FONT_SIZE_SCALE_RATIO];
    }
    
    CCScrollView *gridScroll = [[CCScrollView alloc] initWithContentNode:grid];
    [gridScroll setAnchorPoint:ccp(0.0f, 0.0f)];
    [gridScroll setPosition:ccp(0, 0)];
    [gridScroll setPagingEnabled:NO];
    [gridScroll setHorizontalScrollEnabled:NO];
    [self addChild:gridScroll z:0 name:@"Grid Scroll"];
}

- (void)onBackClicked:(id)sender
{
    [GameSupport playSound:BUTTON_CLICK];
    score = 0;
    CCTransition* t = [CCTransition transitionFadeWithDuration:0.5f];
    t.outgoingSceneAnimated = YES;
    t.incomingSceneAnimated = YES;
    // back to intro scene with transition
    [[CCDirector sharedDirector] replaceScene:[MainMenuScene scene]
                               withTransition:t];
}

- (void)onNextClicked{
    [GameSupport playSound:BUTTON_CLICK];
    CCTransition* t = [CCTransition transitionFadeWithDuration:0.5f];
    t.outgoingSceneAnimated = YES;
    t.incomingSceneAnimated = YES;
    
    int currentScore = [[[GameManager sharedManager] getCurrentPlayer] score];
    [[[GameManager sharedManager] getCurrentPlayer] setScore:(currentScore+score)];
    [[[GameManager sharedManager] getCurrentPlayer] resetResult];
    
//    NSLog(@"NextClicked-CurrentRound: %d - Total R: %d", [[GameManager sharedManager] currentRound], [[[GameManager sharedManager] match] totalRounds]);
//    NSLog(@"NextClicked-CurrentPlayer: %@ - Total P: %d", [[[GameManager sharedManager] getCurrentPlayer] name], [[[[GameManager sharedManager] match] players] count]);
//    NSLog(@"NextClicked-roundEnded=%d", [[GameManager sharedManager] roundEnded]);
    
    if ([[GameManager sharedManager] gameOver]){
        // Game over
        NSLog(@"GAME OVER");
        GameOverPopup* popup = [GameOverPopup spriteWithImageNamed:@"DualResultBackground.png"];
        [self addChild:popup z:2];
        popup.positionType = CCPositionTypeNormalized;
        popup.position = ccp(0.5f, 0.5f);
        [popup loadInScene:self];
        
        // Remove all buttons
        [self removeChildByName:@"nextButton"];
        [self removeChildByName:@"backButton"];

        return;
    }
    
    if ([[GameManager sharedManager] roundEnded]){
        // round ended - Back to categories
        NSLog(@"NEXT ROUND");
        [[GameManager sharedManager] nextRound];
        [[CCDirector sharedDirector] replaceScene:[CategoriesScene scene]
                                   withTransition:t];
    } else {
        // next player - Back to game scene
        NSLog(@"NEXT PLAYER - category:%@", [[[[GameManager sharedManager] match] category] name]);
        [[GameManager sharedManager] nextPlayer];
        [[CCDirector sharedDirector] replaceScene:[GameScene scene]
                                   withTransition:t];
    }
}

@end
