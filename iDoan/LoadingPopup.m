//
//  LoadingPopup.m
//  iDoan
//
//  Created by Duc Nguyen on 2014-06-23.
//  Copyright (c) 2014 Tic Toc Prod. All rights reserved.
//

#import "LoadingPopup.h"
#import "ConstantManager.h"

@interface LoadingPopup(){
    CCScene* parent;
}

@end

@implementation LoadingPopup

- (void) loadInScene:(CCScene *)p{
    // Init class variables
    parent = p;
    
    // Title label
    CCLabelTTF *title= [CCLabelTTF labelWithString:@"Loading ..." fontName:[ConstantManager MAIN_FONT_NAME] fontSize:50.0f * [ConstantManager FONT_SIZE_SCALE_RATIO]];
    title.positionType = CCPositionTypeNormalized;
    title.position = ccp(0.5f, 0.5f);
    [self addChild:title z:0 name:@"title"];
    
    CCLabelTTF* tip = [CCLabelTTF labelWithString:[ConstantManager TIP_TO_PLAY] fontName:[ConstantManager MAIN_FONT_NAME] fontSize:20*[ConstantManager FONT_SIZE_SCALE_RATIO] dimensions:CGSizeMake([CCDirector sharedDirector].viewSize.width, [CCDirector sharedDirector].viewSize.height * 1 / 3)];
    tip.positionType = CCPositionTypeNormalized;
    tip.position = ccp(0.5f, 0.9f);
    tip.horizontalAlignment = CCTextAlignmentCenter;
    tip.verticalAlignment = CCTextAlignmentCenter;
    [self addChild:tip z:1 name:@"tip"];
    
    CCSprite* wrapper = [CCSprite spriteWithImageNamed:@"Wrapper.png"];
    wrapper.scaleX = [CCDirector sharedDirector].viewSize.width / wrapper.contentSize.width;
    wrapper.scaleY = (tip.contentSize.height / wrapper.contentSize.height) * 2/3;
    wrapper.positionType = CCPositionTypeNormalized;
    wrapper.position = tip.position;
    [self addChild:wrapper z:0 name:@"wrapper"];
}

- (void) closePopup {
    if (parent != nil){
        [parent removeChild:self];
    }
}

@end
